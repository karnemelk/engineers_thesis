from django.shortcuts import render, redirect
from apps.authentication.forms import SignUpForm
from django.contrib.auth import login, authenticate, logout as auth_logout


def register(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            passwd = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=passwd)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})


def logout(request):
    auth_logout(request)
    return redirect('index')
