from django.urls import path
from apps.authentication import views
from django.contrib.auth import views as auth

urlpatterns = [
    path('login/', auth.login, {'template_name': 'login.html'}, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name='logout')
]