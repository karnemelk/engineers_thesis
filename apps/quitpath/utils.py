from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.shortcuts import get_object_or_404

from apps.general.models import Notification
from apps.quitpath.models import Quitpath, Breakpoint, Trophy


MONTHS = {
    1: 'Styczeń',
    2: 'Luty',
    3: 'Marzec',
    4: 'Kwiecień',
    5: 'Maj',
    6: 'Czerwiec',
    7: 'Lipiec',
    8: 'Sierpień',
    9: 'Wrzesień',
    10: 'Październik',
    11: 'Listopad',
    12: 'Grudzień'
}


def get_longest_time(id: int):
    quitpath = get_object_or_404(Quitpath, pk=id)

    now = datetime.now()

    longest = (now, quitpath.last_breakpoint_date)
    breakpoints = Breakpoint.objects.filter(quitpath=quitpath).order_by('date')

    for i in range(1, len(breakpoints)):
        rel_delta = (breakpoints[i].date, breakpoints[i - 1].date)

        if (rel_delta[0] - rel_delta[1]).total_seconds() > (longest[0] - longest[1]).total_seconds():
            longest = rel_delta

    return relativedelta(longest[0], longest[1]), (now - quitpath.last_breakpoint_date).total_seconds()


def get_dates(id: int):
    quitpath = get_object_or_404(Quitpath, pk=id)

    now = datetime.now()
    delta = relativedelta(now, quitpath.last_breakpoint_date)
    delta = {
        'years': delta.years,
        'months': delta.months,
        'days': delta.days,
        'hours': delta.hours,
        'minutes': delta.minutes,
        'seconds': delta.seconds
    }

    longest, actual_total_seconds = get_longest_time(id)

    longest = {
        'years': longest.years,
        'months': longest.months,
        'days': longest.days,
        'hours': longest.hours,
        'minutes': longest.minutes,
        'seconds': longest.seconds
    }

    return {
        'delta': delta,
        'longest': longest,
        'actual_total_seconds': actual_total_seconds
    }


def prepare_trophies(path):
    daytime = 24 * 3600
    week = 7 * daytime
    month = 31 * daytime
    year = 365 * daytime

    trophies = [
        {
            'name': '1 dzień',
            'time': daytime,
            'quitpath': path,
            'icon': '/static/trophies-icons/1day.png'
        },
        {
            'name': '3 dni',
            'time': 3 * daytime,
            'quitpath': path,
            'icon': '/static/trophies-icons/3days.png'
        },
        {
            'name': '1 tydzień',
            'time': week,
            'quitpath': path,
            'icon': '/static/trophies-icons/1week.png'
        },
        {
            'name': '10 dni',
            'time': 10 * daytime,
            'quitpath': path,
            'icon': '/static/trophies-icons/10days.png'
        },
        {
            'name': '2 tygodnie',
            'time': 2 * week,
            'quitpath': path,
            'icon': '/static/trophies-icons/2weeks.png'
        },
        {
            'name': '1 miesiąc',
            'time': month,
            'quitpath': path,
            'icon': '/static/trophies-icons/1month.png'
        },
        {
            'name': '3 miesiące',
            'time': 3 * month,
            'quitpath': path,
            'icon': '/static/trophies-icons/3months.png'
        },
        {
            'name': '1 rok',
            'time': year,
            'quitpath': path,
            'icon': '/static/trophies-icons/1year.png'
        },
        {
            'name': '3 lata',
            'time': 3 * year,
            'quitpath': path,
            'icon': '/static/trophies-icons/3years.png'
        }
    ]

    for trophy in trophies:
        Trophy.objects.create(**trophy)


def check_trophy(trophy):
    if trophy.achieved:
        return False

    now = datetime.now()
    latest = trophy.quitpath.last_breakpoint_date
    total_seconds = (now - latest).total_seconds()

    if total_seconds > trophy.time:
        return True
    else:
        return False


def reset_trophies(quitpath):
    actual_time = (datetime.now() - quitpath.last_breakpoint_date).total_seconds()
    for trophy in Trophy.objects.filter(quitpath=quitpath).order_by('time'):
        if trophy.time > actual_time:
            trophy.achieved = False
        else:
            trophy.achieved = True
        trophy.save()


def find_best_trophy(quitpath):
    best_trophy = None
    actual_time = (datetime.now() - quitpath.last_breakpoint_date).total_seconds()

    for trophy in Trophy.objects.filter(quitpath=quitpath).order_by('time').values():
        best_trophy = trophy
        if trophy['time'] > actual_time:
            break

    return best_trophy


def create_notification(user, text, url):
    _ = Notification.objects.create(user=user, text=text, url=url)
