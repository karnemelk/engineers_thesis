import json
from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from dateutil.relativedelta import relativedelta
from django.urls import reverse

from apps.quitpath.models import Quitpath, Trophy
from apps.quitpath.utils import get_longest_time, get_dates, MONTHS, find_best_trophy, reset_trophies
from .forms import QuitpathForm, Breakpoint, BreakpointForm


@login_required
def paths(request):
    paths = list()

    for path in Quitpath.objects.filter(user=request.user):
        _, actual_total_seconds = get_longest_time(path.pk)

        paths.append({
            'data': get_dates(path.pk),
            'path': path,
            'trophy': find_best_trophy(path),
            'actual_total_seconds': actual_total_seconds
        })

    context = {
        'paths': paths
    }
    return render(request, 'paths.html', context)


@login_required
def new_path(request):
    if request.method == 'POST':
        form = QuitpathForm(request.POST)
        if form.is_valid():
            form.start_path(request.user)
            return redirect('paths')
    else:
        form = QuitpathForm()
    context = {
        'form': form
    }
    return render(request, 'new_path.html', context)


@login_required
def quitpath(request, id):
    quitpath = get_object_or_404(Quitpath, pk=id)

    if request.method == 'POST':
        form = BreakpointForm(request.POST)
        if form.is_valid():
            form.create_breakpoint(quitpath)
            reset_trophies(quitpath)
            return redirect('quitpath', id=id)
    else:
        form = BreakpointForm()

    now = datetime.now()
    delta = relativedelta(now, quitpath.last_breakpoint_date)

    if request.GET.get('month') and request.GET.get('year'):
        try:
            month = int(request.GET['month'])
            year = int(request.GET['year'])
        except:
            month = now.month
            year = now.year
    else:
        month = now.month
        year = now.year
    cal_date = datetime(day=1, month=month, year=year)

    prev_month = cal_date - relativedelta(months=1)
    next_month = cal_date + relativedelta(months=1)

    calendar = []
    row = list()

    breakpoints = {}

    for breakpoint in Breakpoint.objects.filter(quitpath=quitpath):
        date = '{}-{}-{}'.format(breakpoint.date.day, breakpoint.date.month, breakpoint.date.year)

        try:
            _ = breakpoints[date]
        except:
            breakpoints[date] = []

        breakpoints[date].append(breakpoint)

    while cal_date.month != next_month.month:
        date = '{}-{}-{}'.format(cal_date.day, cal_date.month, cal_date.year)
        day = {
            'date': date,
            'day': cal_date.day,
            'breakpoints': sorted(breakpoints.get(date, list()), key=lambda x: x.date),
            'count': len(breakpoints.get(date, list()))
        }

        if cal_date == datetime(day=now.day, month=now.month, year=now.year):
            day['today'] = True

        row.append(day)

        if cal_date.weekday() == 6:
            calendar.append(row)
            row = list()

        cal_date += timedelta(days=1)

    if row:
        calendar.append(row)

    if len(calendar[0]) != 7:
        calendar[0] = [None for _ in range(7 - len(calendar[0]))] + calendar[0]
    if len(calendar[-1]) != 7:
        calendar[-1] = calendar[-1] + [None for _ in range(7 - len(calendar[-1]))]

    longest, actual_total_seconds = get_longest_time(id)

    context = {
        'id': id,
        'quitpath': quitpath,
        'today': {
            'month': MONTHS[month],
            'year': year
        },
        'begin_date': quitpath.begin_date.strftime('%d-%m-%Y'),
        'last_breakpoint_date': quitpath.last_breakpoint_date.strftime('%H:%M %d-%m-%Y'),
        'abst': delta,
        'calendar': calendar,
        'next_month': next_month,
        'prev_month': prev_month,
        'longest': longest,
        'actual_total_seconds': actual_total_seconds,
        'form': form,
        'trophies': Trophy.objects.filter(quitpath=quitpath).order_by('time').values()
    }

    return render(request, 'quitpath.html', context)


@login_required
def delete_breakpoint(request, quitpath_id, breakpoint_id):
    breakpoint = get_object_or_404(Breakpoint, pk=breakpoint_id)
    quitpath = get_object_or_404(Quitpath, pk=quitpath_id)

    breakpoint.delete()

    quitpath.last_breakpoint_date = Breakpoint.objects.filter(quitpath=quitpath).latest('date').date
    quitpath.save()
    reset_trophies(quitpath)

    return HttpResponseRedirect(reverse('quitpath', kwargs={'id': quitpath_id}))


@login_required
def gettime(request, id):
    return HttpResponse(json.dumps(get_dates(id)), content_type='application/json')


@login_required
def get_best_trophy(request, id):
    path = get_object_or_404(Quitpath, pk=id)

    best_trophy = find_best_trophy(path)
    best_trophy['actual_total_seconds'] = (datetime.now() - path.last_breakpoint_date).total_seconds()

    return HttpResponse(json.dumps(best_trophy), content_type='application/json')


@login_required
def delete_quitpath(request, id):
    quitpath = get_object_or_404(Quitpath, pk=id)
    quitpath.delete()
    return HttpResponseRedirect(reverse('paths'))
