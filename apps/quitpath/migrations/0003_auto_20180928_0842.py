# Generated by Django 2.0 on 2018-09-28 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quitpath', '0002_auto_20180923_1936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addiction',
            name='icon',
            field=models.ImageField(upload_to='addictions_icons/'),
        ),
    ]
