from django.apps import AppConfig


class QuitpathConfig(AppConfig):
    name = 'apps.quitpath'
    verbose_name = 'Zarządzanie nałogami'
