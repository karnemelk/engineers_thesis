from django.db import models
from django.contrib.auth.models import User


class Addiction(models.Model):
    name = models.CharField(max_length=1000)
    icon = models.ImageField(upload_to='addictions_icons/')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Nałóg'
        verbose_name_plural = 'Nałogi'


class Quitpath(models.Model):
    begin_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='quitpath_user')
    addiction = models.ForeignKey(Addiction, on_delete=models.CASCADE, related_name='quitpath_addiction')
    last_breakpoint_date = models.DateTimeField(auto_now_add=False)
    motive = models.TextField()

    def __str__(self):
        return 'quitpath_{}'.format(self.pk)

    class Meta:
        verbose_name = 'Ścieżka'
        verbose_name_plural = 'Ścieżki'


class Trophy(models.Model):
    name = models.CharField(max_length=100)
    time = models.PositiveIntegerField(default=0)
    quitpath = models.ForeignKey(Quitpath, on_delete=models.CASCADE, related_name='trophy_quitpath')
    icon = models.CharField(max_length=100, default='')
    achieved = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Trofeum'
        verbose_name_plural = 'Trofea'


class Breakpoint(models.Model):
    date = models.DateTimeField(auto_now_add=False)
    quitpath = models.ForeignKey(Quitpath, on_delete=models.CASCADE, related_name='breakpoint_quitpath')

    def __str__(self):
        return '{}-{}-{}'.format(self.date.day, self.date.month, self.date.year)
