from django.contrib import admin
from django.utils.html import format_html

from .models import Addiction, Trophy


@admin.register(Addiction)
class AddictionAdmin(admin.ModelAdmin):
    list_display = ['name', 'icon_tag']

    def icon_tag(self, obj):
        return format_html('<img src="/media/{}" />'.format(obj.icon))

    icon_tag.short_description = 'Icon'

