from django.urls import path
from apps.quitpath import views

urlpatterns = [
    path('', views.paths, name='paths'),
    path('new', views.new_path, name='new_path'),
    path('quitpath/<int:id>', views.quitpath, name='quitpath'),
    path('gettime/<int:id>', views.gettime, name='gettime'),
    path('delete_breakpoint/<int:quitpath_id>/<int:breakpoint_id>', views.delete_breakpoint, name='delete_breakpoint'),
    path('get_best_trophy/<int:id>', views.get_best_trophy, name='get_best_trophy'),
    path('delete_quitpath/<int:id>', views.delete_quitpath, name='delete_quitpath')
]