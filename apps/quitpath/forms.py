from django import forms
from datetime import datetime

from django.utils.html import format_html

from apps.quitpath.models import Quitpath, Addiction, Breakpoint, Trophy
from apps.quitpath.utils import prepare_trophies


def leading_zero(number: int):
    return '{}{}'.format('' if number > 9 else '0', number)


class QuitpathForm(forms.Form):
    addiction = forms.ChoiceField(
        widget=forms.RadioSelect(),
        choices=[
            (
                addiction.pk,
                format_html(
                    '<span><img src="/media/{}" width="42" height="42"/> &nbsp;&nbsp;&nbsp;&nbsp;{}</span>'
                    .format(addiction.icon, addiction.name))
            )
            for addiction in Addiction.objects.all()
        ]
    )

    time = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Wybierz godzinę',
                'data-default': '{}:{}'.format(leading_zero(datetime.now().hour), leading_zero(datetime.now().minute)),
                'value': '{}:{}'.format(leading_zero(datetime.now().hour), leading_zero(datetime.now().minute)),
            }
        )
    )

    date = forms.CharField(
        label='Wybierz datę',
        widget=forms.DateInput(
            attrs={
                'class': 'input-top form-control',
                'placeholder': 'Wybierz datę',
                'data-provide': 'datepicker',
                'data-date-autoclose': 'true',
                'data-date-format': 'dd/mm/yyyy',
                'data-date-clear-btn': 'true',
                'data-date-language': 'pl',
                'value': '{}/{}/{}'.format(
                    leading_zero(datetime.now().day),
                    leading_zero(datetime.now().month),
                    datetime.now().year
                ),
            },
            format='%d/%m/%Y'
        )
    )

    motive = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows': '4',
                'cols': '40',
                'placeholder': 'np. oszczędność czasu, pieniędzy',
            }
        )
    )

    def clean_addiction(self):
        addiction = self.cleaned_data['addiction']
        try:
            addiction = Addiction.objects.get(pk=int(addiction))
        except:
            raise forms.ValidationError('Wybierz poprawny nałóg')
        return addiction

    def clean_time(self):
        try:
            time = self.cleaned_data['time']
            format_str = '%H:%M'
            time = datetime.strptime(time, format_str)
        except:
            raise forms.ValidationError('Wprowadź poprawną godzinę')
        return time

    def clean_date(self):
        try:
            date = self.cleaned_data['date']
            format_str = '%d/%m/%Y'
            date = datetime.strptime(date, format_str)
        except:
            raise forms.ValidationError('Wprowadź poprawną datę')

        try:
            time = self.cleaned_data['time']
            full_date = datetime(date.year, date.month, date.day, time.hour, time.minute)
            if full_date > datetime.now():
                raise Exception
        except:
            raise forms.ValidationError('Data powinna być wcześniejsza niż obecna')

        return date

    def clean_motive(self):
        motive = self.cleaned_data['motive']
        if len(motive) < 6:
            raise forms.ValidationError('Tekst powinien zawierać minimum 6 znaków')
        return motive

    def start_path(self, user):
        addiction = self.cleaned_data['addiction']
        time = self.cleaned_data['time']
        date = self.cleaned_data['date']
        motive = self.cleaned_data['motive']

        full_date = datetime(date.year, date.month, date.day, time.hour, time.minute)

        path = Quitpath.objects.create(user=user, addiction=addiction, motive=motive, last_breakpoint_date=full_date)
        breakpoint = Breakpoint.objects.create(date=str(full_date), quitpath=path)

        prepare_trophies(path)


class BreakpointForm(forms.Form):
    time = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Wybierz godzinę',
                'data-default': '{}:{}'.format(leading_zero(datetime.now().hour), leading_zero(datetime.now().minute)),
                'value': '{}:{}'.format(leading_zero(datetime.now().hour), leading_zero(datetime.now().minute)),
            }
        )
    )

    date = forms.CharField(
        label='Wybierz datę',
        widget=forms.DateInput(
            attrs={
                'class': 'input-top form-control',
                'placeholder': 'Wybierz datę',
                'data-provide': 'datepicker',
                'data-date-autoclose': 'true',
                'data-date-format': 'dd/mm/yyyy',
                'data-date-clear-btn': 'true',
                'data-date-language': 'pl',
                'value': '{}/{}/{}'.format(
                    leading_zero(datetime.now().day),
                    leading_zero(datetime.now().month),
                    datetime.now().year
                ),
            },
            format='%d/%m/%Y'
        )
    )

    def clean_time(self):
        try:
            time = self.cleaned_data['time']
            format_str = '%H:%M'
            time = datetime.strptime(time, format_str)
        except:
            raise forms.ValidationError('Wprowadź poprawną godzinę')
        return time

    def clean_date(self):
        try:
            date = self.cleaned_data['date']
            format_str = '%d/%m/%Y'
            date = datetime.strptime(date, format_str)
        except:
            raise forms.ValidationError('Wprowadź poprawną datę')

        try:
            time = self.cleaned_data['time']
            full_date = datetime(date.year, date.month, date.day, time.hour, time.minute)
            if full_date > datetime.now():
                raise Exception
        except:
            raise forms.ValidationError('Data powinna być wcześniejsza niż obecna')

        return date

    def create_breakpoint(self, path):
        time = self.cleaned_data['time']
        date = self.cleaned_data['date']

        full_date = datetime(date.year, date.month, date.day, time.hour, time.minute)
        _ = Breakpoint.objects.create(date=str(full_date), quitpath=path)
        latest = Breakpoint.objects.filter(quitpath=path).latest('date')

        path.last_breakpoint_date = latest.date
        path.save()

