from django.urls import path
from apps.general import views

urlpatterns = [
    path('', views.index, name='index'),
    path('notification/<int:id>', views.notification, name='notification'),
    path('get_notifications', views.get_notifications, name='get_notifications'),
]
