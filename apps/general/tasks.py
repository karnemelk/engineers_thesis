from datetime import datetime

from celery import shared_task
from django.contrib.auth.models import User
from django.urls import reverse

from apps.general.models import Notification
from apps.quitpath.models import Quitpath, Trophy
from apps.quitpath.utils import check_trophy, create_notification


@shared_task
def get_notifications():
    for user in User.objects.all():
        for quitpath in Quitpath.objects.filter(user=user):
            for trophy in sorted(Trophy.objects.filter(quitpath=quitpath), key=lambda x: x.time):
                if check_trophy(trophy):
                    trophy.achieved = True
                    trophy.save()
                    text = 'Nowe osiągnięcie:<br>trofeum <b>{}</b> w ścieżce <b>{}</b><br>Gratulacje!'.format(
                        trophy.name,
                        quitpath.addiction.name
                    )
                    url = reverse('quitpath', kwargs={'id': quitpath.pk})
                    create_notification(user=user, text=text, url=url)


@shared_task
def reduce_notifications():
    for notification in Notification.objects.all():
        time = (datetime.now() - notification.created).days
        if time >= 7:
            notification.delete()
