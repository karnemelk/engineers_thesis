from django.contrib.auth.models import User
from django.db import models

from datetime import datetime


class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notification')
    text = models.TextField()
    read = models.BooleanField(default=False)
    url = models.CharField(max_length=100)
    created = models.DateTimeField(default=datetime.now())
