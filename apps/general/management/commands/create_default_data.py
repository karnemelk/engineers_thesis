from django.core.management import BaseCommand

from apps.forum.models import Category, Forum
from apps.motivation.models import Motivation
from apps.quitpath.models import Addiction
from apps.specialists.models import Specialist


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.__create_addictions()
        self.__create_motivations()
        self.__create_forum_components()
        self.__create_specialists()

    def __create_addictions(self):
        print('- Creating default addictions -')
        addictions = [
            {
                'name': 'Papierosy',
                'icon': 'addictions_icons/cigarete.png'
            },
            {
                'name': 'Alkohol',
                'icon': 'addictions_icons/alcohol.png'
            },
            {
                'name': 'Narkotyki',
                'icon': 'addictions_icons/narco.png'
            },
            {
                'name': 'Pornografia',
                'icon': 'addictions_icons/adult.png'
            },
            {
                'name': 'Gry',
                'icon': 'addictions_icons/games.png'
            },
            {
                'name': 'Telewizja',
                'icon': 'addictions_icons/tv.png'
            },
            {
                'name': 'Facebook',
                'icon': 'addictions_icons/fb.png'
            },
            {
                'name': 'Słodycze',
                'icon': 'addictions_icons/sweets.png'
            },
            {
                'name': 'Kawa',
                'icon': 'addictions_icons/coffee.png'
            },
            {
                'name': 'Hazard',
                'icon': 'addictions_icons/cash.png'
            },
            {
                'name': 'Przeklinanie',
                'icon': 'addictions_icons/swearword.png'
            },
            {
                'name': 'Kłamstwo',
                'icon': 'addictions_icons/lie.png'
            },
            {
                'name': 'Inne',
                'icon': 'addictions_icons/other.png'
            },
        ]
        count = 0
        for addiction in addictions:
            if len(Addiction.objects.filter(**addiction)) == 0:
                count += 1
                Addiction.objects.create(**addiction)
        print('Created {} new addictions'.format(count))

    def __create_motivations(self):
        print('- Creating default motivations -')
        motivations = [
            {
                'text': 'Dwa najważniejsze dni Twojego życia to ten, w którym się urodziłeś oraz ten, w którym dowiedziałeś się, po co.',
                'author': 'Mark Twain'
            },
            {
                'text': 'Pudłujesz 100% strzałów, jeśli w ogóle ich nie wykonujesz.',
                'author': 'Wayne Gretzky'
            },
            {
                'text': 'Najlepszą zemstą jest ogromny sukces.',
                'author': 'Frank Sinatra'
            },
            {
                'text': 'Twój czas jest ograniczony, więc nie marnuj go na byciem kimś, kim nie jesteś.',
                'author': 'Steve Jobs'
            },
            {
                'text': 'Aby zerwać z nawykiem, wyrób sobie inny, który go wymaże.',
                'author': 'Mark Twain'
            },
        ]
        count = 0
        for motivation in motivations:
            if len(Motivation.objects.filter(**motivation)) == 0:
                count += 1
                Motivation.objects.create(**motivation)
        print('Created {} new motivations'.format(count))

    def __create_forum_components(self):
        print('- Creating default forum components -')
        categories = [
            {
                'name': 'Ogólne',
                'forum_list': [
                    {
                        'name': 'Zasady korzystania forum',
                        'description': '.. instrukcje dotyczące korzystania z forum aplikacji'
                    },
                    {
                        'name': 'Nowości',
                        'description': '... bliższe lub dalsze zmiany'
                    },
                ]
            },
            {
                'name': 'Nałogi',
                'forum_list': [
                    {
                        'name': 'Nałóg papierosowy',
                        'description': '... różne porady na temat pozbycia się nałogu papierosowego'
                    },
                    {
                        'name': 'Nałóg alkoholowy',
                        'description': '... różne porady na temat pozbycia się nałogu alkoholowego'
                    },
                ]
            },
            {
                'name': 'Inne',
                'forum_list': [
                    {
                        'name': 'Świadectwa',
                        'description': '... czyli historie innych użytkowników o tym, jak pokonali swoje nałogi'
                    },
                    {
                        'name': 'Inne',
                        'description': '... czyli inne wątki związane mniej lub bardziej z kontekstem aplikacji'
                    },
                ]
            },
        ]
        count_categories = 0
        count_forum = 0
        for category in categories:
            if len(Category.objects.filter(name=category['name'])) == 0:
                count_categories += 1
                cat = Category.objects.create(name=category['name'])
                for forum in category['forum_list']:
                    forum['category'] = cat
                    if len(Forum.objects.filter(**forum)) == 0:
                        count_forum += 1
                        Forum.objects.create(**forum)

        print('Created {} new categories'.format(count_categories))
        print('Created {} new foras'.format(count_forum))

    def __create_specialists(self):
        print('- Creating default specialists -')
        specialists = [
            {
                'name': 'Kacper Filip',
                'surname': 'Chociej',
                'phone': '507 937 406',
                'address': 'ul. Palmowa 8/2',
                'postal': '15-795',
                'city': 'Białystok',
                'photo': 'specialists/default.png'
            },
        ]
        count = 0
        for specialist in specialists:
            if len(Specialist.objects.filter(**specialist)) == 0:
                count += 1
                Specialist.objects.create(**specialist)
        print('Created {} new specialists'.format(count))
