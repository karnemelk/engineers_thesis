import datetime
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils.html import format_html

from apps.general.models import Notification
from apps.motivation.views import motivation


def index(request):
	item_login = {
		'id': 'id-login',
		'title': 'Logowanie',
		'url': '/login',
		'icon': '/static/index-icons/login.png',
		'text': 'Zaloguj się i zwalcz swój nałóg!'
	}
	item_register = {
		'id': 'id-register',
		'title': 'Rejestracja',
		'url': '/register',
		'icon': '/static/index-icons/register.png',
		'text': 'Nie masz konta? Załóż je teraz i rozpocznij walkę ze złymi nawykami!'
	}
	item_forum = {
		'id': 'id-forum',
		'title': 'Forum',
		'url': '/forum',
		'icon': '/static/index-icons/forum.png',
		'text': 'Dziel się swoimi radami i korzystaj z pomocy innych użytkowników.'
	}
	item_path = {
		'id': 'id-path',
		'title': 'Moje ścieżki',
		'url': '/paths',
		'icon': '/static/index-icons/stop.png',
		'text': 'Zarządzaj swoimi ścieżkami wychodzenia z nałogów.'
	}
	item_messages = {
		'id': 'id-messages',
		'title': 'Wiadomości',
		'url': '/messages',
		'icon': '/static/index-icons/messages.png',
		'text': 'Komunikuj się z innymi użytkownikami i przeglądaj wiadomości od nich.'
	}
	item_contacts = {
		'id': 'id-contacts',
		'title': 'Specjaliści',
		'url': '/specialists',
		'icon': '/static/index-icons/phone.png',
		'text': 'Zasięgnij pomocy bezpośrednio u specjalistów w dziedzinie.'
	}
	item_admin = {
		'id': 'id-admin',
		'title': 'Panel administratora',
		'url': '/admin',
		'icon': '/static/index-icons/admin.png',
		'text': 'Zarządzanie serwisem.'
	}

	if request.user.is_authenticated:
		navs = [item_forum, item_path, item_messages, item_contacts]
		if request.user.is_superuser:
			navs += [item_admin, ]
	else:
		navs = [item_login, item_register]

	context = {
		'navs': navs,
		'quota': json.loads(motivation(request).content)
	}

	if request.user.is_authenticated:
		context['notifications'] = sorted(Notification.objects.filter(user=request.user), key=lambda x: x.created, reverse=True)

	return render(request, 'index.html', context)


@login_required()
def notification(request, id):
	notif = get_object_or_404(Notification, pk=id)
	if request.user != notif.user:
		return HttpResponseForbidden()
	if not notif.read:
		notif.read = True
		notif.save()
	return HttpResponseRedirect(notif.url)


@login_required
def get_notifications(request):
	notifications = list(sorted(Notification.objects.filter(user=request.user).values(), key=lambda x: x['created'], reverse=True))
	for i in range(len(notifications)):
		date = notifications[i]['created']
		notifications[i]['created'] = '{}:{} {}/{}/{}'.format(date.hour, date.minute, date.day, date.month, date.year)
	return HttpResponse(json.dumps(notifications), content_type='application/json')
