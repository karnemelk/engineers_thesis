from django import forms
from .models import Thread, Post


class NewThreadForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': 'Nazwa wątku',
            'class': 'form-control'
    }))
    content = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': '4',
            'cols': '40',
            'placeholder': 'Treść postu',
            'class': 'form-control'
    }))

    def clean_name(self):
        name = self.cleaned_data['name']

        if len(name) < 5:
            raise forms.ValidationError('Nazwa nowego wątku powinna mieć przynajmniej 5 znaków')

        return name

    def clean_content(self):
        content = self.cleaned_data['content']

        if len(content) < 10:
            raise forms.ValidationError('Zawartość wiadomości powinna mieć przynajmniej 10 znaków')

        return content

    def start_thread(self, forum, user):
        thread = Thread.objects.create(name=self.cleaned_data['name'], forum=forum, poster=user)
        post = Post.objects.create(poster=user, thread=thread, content=self.cleaned_data['content'], is_first=True)
        return thread


class NewPostForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': '3',
            'cols': '40',
            'placeholder': 'Treść postu',
            'class': 'form-control'
    }))

    def clean_content(self):
        content = self.cleaned_data['content']

        if len(content) < 10:
            raise forms.ValidationError('Zawartość wiadomości powinna mieć przynajmniej 10 znaków')

        return content

    def add_post(self, user, thread):
        post = Post.objects.create(poster=user, thread=thread, content=self.cleaned_data['content'])
        return thread
