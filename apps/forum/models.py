from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
	name = models.CharField(max_length=256, unique=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Kategoria'
		verbose_name_plural = 'Kategorie'


class Forum(models.Model):
	name = models.CharField(max_length=256, unique=True)
	category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
	description = models.TextField()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Forum'
		verbose_name_plural = 'Fora'


class Thread(models.Model):
	name = models.CharField(max_length=256)
	poster = models.ForeignKey(User, on_delete=models.CASCADE)
	forum = models.ForeignKey(Forum, on_delete=models.CASCADE)
	date = models.DateField(auto_now=True)
	
	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Wątek'
		verbose_name_plural = 'Wątki'


class Post(models.Model):
	poster = models.ForeignKey(User, on_delete=models.CASCADE)
	thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
	content = models.TextField()
	date = models.DateTimeField(auto_now_add=True)
	is_first = models.BooleanField(default=False)

	def __str__(self):
		return '#{}'.format(self.pk)

	class Meta:
		verbose_name = 'Post'
		verbose_name_plural = 'Posty'
