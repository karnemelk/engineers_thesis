from django.apps import AppConfig


class ForumConfig(AppConfig):
    name = 'apps.forum'
    verbose_name = 'Zarządzanie forum'
