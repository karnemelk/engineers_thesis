from django.urls import path
from apps.forum import views

urlpatterns = [
    path('', views.index, name='main_forum'),
    path('<int:id>/', views.forum, name='forum'),
    path('<int:id>/thread_start/', views.thread_start, name='thread_start'),
    path('thread/<int:id>/', views.thread, name='thread'),
]