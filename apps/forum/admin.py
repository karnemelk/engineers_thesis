from django.contrib import admin
from .models import Category, Forum, Thread, Post


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	pass


@admin.register(Forum)
class ForumAdmin(admin.ModelAdmin):
	pass


@admin.register(Thread)
class ThreadAdmin(admin.ModelAdmin):
	pass


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	pass
