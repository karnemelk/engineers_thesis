from django.shortcuts import render, redirect
from .models import Category, Forum, Thread, Post
from django.shortcuts import get_object_or_404
from .forms import NewThreadForm, NewPostForm


def index(request):
	categories = []

	for category in Category.objects.all():
		forums = Forum.objects.filter(category=category)
		if forums:
			categories.append({
				'name' : category.name,
				'forums' : forums
			})
	if Forum.objects.filter(category=None):
		categories.append({
			'name' : 'Inne',
			'forums' : Forum.objects.filter(category=None)
		})

	context = {
		'categories' : categories
	}

	return render(request, 'main_forum.html', context)

def forum(request, id):
	forum = get_object_or_404(Forum, pk=id)
	threads = Thread.objects.filter(forum=forum)
	context = {
		'forum' : forum,
		'threads' : threads
	}
	return render(request, 'forum.html', context)


def thread_start(request, id):
	forum = get_object_or_404(Forum, pk=id)
	if request.method == 'POST':
		form = NewThreadForm(request.POST)
		if form.is_valid():
			thread = form.start_thread(forum, request.user)
			return redirect('/forum/thread/{}/'.format(thread.pk))
	else:
		form = NewThreadForm()
	context = {
		'form': form,
		'id': id
	}
	return render(request, 'thread_start.html', context)


def thread(request, id):
	thread = get_object_or_404(Thread, pk=id)
	posts = Post.objects.filter(thread=thread)

	if request.method == 'POST':
		form = NewPostForm(request.POST)
		if form.is_valid():
			thread = form.add_post(request.user, thread)
			# return redirect('/forum/thread/{}/'.format(id))
	else:
		form = NewPostForm()

	context = {
		'thread': thread,
		'posts': posts,
		'form': form
	}
	return render(request, 'thread.html', context)
