from django.db import models


class Motivation(models.Model):
	text = models.CharField(max_length=5000)
	author = models.CharField(max_length=100, null=True, blank=True)

	class Meta:
		verbose_name = 'Tekst motywacyjny'
		verbose_name_plural = 'Teksty motywacyjne'

	def __str__(self):
		return self.text
