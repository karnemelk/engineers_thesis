from django.contrib import admin
from apps.motivation.models import Motivation


@admin.register(Motivation)
class MotivationAdmin(admin.ModelAdmin):
	list_display = ['text', 'author']
