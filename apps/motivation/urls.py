from django.urls import path
from apps.motivation import views

urlpatterns = [
    path('get', views.motivation, name='motivation'),
]