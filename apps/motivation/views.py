from random import randint

import json

from django.forms import model_to_dict
from django.http import HttpResponse

from apps.motivation.models import Motivation


def motivation(request):
    try:
        rand = randint(0, len(Motivation.objects.all()) - 1)
        data = model_to_dict(Motivation.objects.all()[rand])

        return HttpResponse(json.dumps(data), content_type='application/json')
    except:
        return HttpResponse('-', content_type='application/json')
