from django.apps import AppConfig


class MotivationConfig(AppConfig):
    name = 'apps.motivation'
    verbose_name = 'Teksty motywacyjne'
