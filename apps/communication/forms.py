from django import forms
from django.contrib.auth.models import User

from .models import Message


class NewMessageForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': '4',
            'cols': '40',
            'placeholder': 'Treść wiadomości',
            'class': 'form-control'
        }))
    receiver = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': 'Odbiorca',
            'class': 'form-control'
    }))

    class Meta:
        model = Message
        fields = ('content', 'receiver')

    def clean_content(self):
        content = self.cleaned_data['content']

        if len(content) < 1:
            raise forms.ValidationError('Zawartość wiadomości nie może być pusta')

        return content

    def clean_receiver(self):
        receiver = self.cleaned_data['receiver']

        try:
            user = User.objects.get(username=receiver)
        except:
            raise forms.ValidationError('Nie ma takiego użytkownika')

        return user

    def send_message(self, shipper):
        message = Message.objects.create(
            content=self.cleaned_data['content'], 
            shipper=shipper, 
            receiver=self.cleaned_data['receiver']
        )
        return message
