from django.urls import path
from apps.communication import views

urlpatterns = [
    path('', views.messages, name='messages'),
    path('new', views.new_message, name='new_message'),
    path('<int:user_id>/', views.show, name='show'),
]