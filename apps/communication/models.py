from django.db import models
from django.contrib.auth.models import User


class Message(models.Model):
    content = models.CharField(max_length=1000)
    shipper = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='shipper')
    receiver = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='receiver')
    created_at = models.DateTimeField(auto_now=True)
    seen = models.BooleanField(default=False)
