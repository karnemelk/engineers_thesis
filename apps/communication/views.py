from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse
from django.shortcuts import redirect, render, HttpResponseRedirect, get_object_or_404

from apps.quitpath.utils import create_notification
from .models import Message
from .forms import NewMessageForm


@login_required
def messages(request):
    conversations = []

    for user in User.objects.all():
        sent = Message.objects.filter(shipper=request.user, receiver=user)
        received = Message.objects.filter(shipper=user, receiver=request.user)

        if len(sent) > 0 or len(received) > 0:
            date = max(message.created_at for message in sent | received)
            conversations.append({
                'user': user,
                'count': len(received.filter(seen=False)),
                'date': date,
            })

    from operator import itemgetter
    conversations = sorted(conversations, key=itemgetter('date'), reverse=True)

    context = {
        'conversations': conversations
    }

    return render(request, 'messages.html', context)


@login_required
def new_message(request):
    context = {}
    user = request.user

    if request.method == "POST":
        form = NewMessageForm(request.POST)
        if form.is_valid():
            message = form.send_message(user)

            text = 'Nowa wiadomość:<br>od <b>{}</b>'.format(request.user.username)
            url = reverse('show', kwargs={'user_id': request.user.pk})
            create_notification(user=message.receiver, text=text, url=url)

            return redirect('/messages/{}/'.format(message.receiver.pk))
    else:
        form = NewMessageForm()

    context.update({
        'form': form
    })
    return render(request, 'new.html', context)


@login_required
def show(request, user_id):
    other_user = get_object_or_404(User, pk=user_id)

    sent = Message.objects.filter(shipper=request.user.pk, receiver=user_id)
    received = Message.objects.filter(shipper=user_id, receiver=request.user.pk)

    for message in received:
        if message.seen is False:
            message.seen = True
            message.save()

    messages = sorted(sent | received, key=lambda message: message.created_at)

    context = {
        'messages': messages,
        'other_user': other_user,
    }

    if request.method == "POST":
        message = Message()
        message.content = request.POST['content']
        message.shipper = request.user
        message.receiver = other_user
        message.save()

        text = 'Nowa wiadomość:<br>od <b>{}</b>'.format(request.user.username)
        url = reverse('show', kwargs={'user_id': request.user.pk})
        create_notification(user=other_user, text=text, url=url)

        return HttpResponseRedirect(reverse('show', kwargs={'user_id': user_id}))

    return render(request, 'show.html', context)

