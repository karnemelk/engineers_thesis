from django.db import models


class Specialist(models.Model):
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    full_name = models.CharField(max_length=400, null=True, blank=True)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    postal = models.CharField(max_length=10)
    city = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='specialists/')

    def __str__(self):
        return "{} {}".format(self.name, self.surname)

    class Meta:
        verbose_name = 'Specjalista'
        verbose_name_plural = 'Specjaliści'

    def save(self, *args, **kwargs):
        self.full_name = '{} {}'.format(self.name, self.surname)
        super(Specialist, self).save(*args, **kwargs)
