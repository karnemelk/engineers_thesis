from django.contrib import admin
from django.utils.html import format_html

from apps.specialists.models import Specialist


@admin.register(Specialist)
class SpecialistsAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'photo_tag', 'phone', 'address', 'postal', 'city']
    readonly_fields = ['full_name']

    def photo_tag(self, obj):
        return format_html('<img src="/media/{}" width="80"/>'.format(obj.photo))

    photo_tag.short_description = 'Zdjęcie'
