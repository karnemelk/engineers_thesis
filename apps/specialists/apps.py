from django.apps import AppConfig


class SpecialistsConfig(AppConfig):
    name = 'apps.specialists'
    verbose_name = 'Zarządzanie specjalistami'
