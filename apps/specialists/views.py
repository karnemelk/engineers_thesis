from operator import itemgetter

from django.shortcuts import render

from apps.specialists.models import Specialist


def specialists(request):
    cities = Specialist.objects.order_by().values_list('city', flat=True).distinct()

    def if_name_contains(input, name):
        return True if input.lower() in name.lower() else False

    if request.GET.get('name'):
        full_name = request.GET['name']
        specialists_list = list(
            spec for spec in Specialist.objects.all() if if_name_contains(full_name, spec.full_name)
        )
        cities_filtered = set(spec.city for spec in specialists_list)
        specialists_list = sorted(
            [{'city': city, 'specialists': list(spec for spec in specialists_list if spec.city == city)} for city in cities_filtered],
            key=itemgetter('city')
        )
    elif request.GET.get('city'):
        city = request.GET['city']
        specialists_list = [{'city': city, 'specialists': Specialist.objects.filter(city=city)}, ]
    else:
        specialists_list = sorted(
            [{'city': city, 'specialists': Specialist.objects.filter(city=city)} for city in cities],
            key=itemgetter('city')
        )

    context = {
        'specialists': specialists_list,
        'cities': cities
    }

    return render(request, 'specialists.html', context)
