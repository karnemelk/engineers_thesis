from django.urls import path
from apps.specialists import views

urlpatterns = [
    path('', views.specialists, name='specialists'),
]