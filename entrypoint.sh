#!/bin/bash

echo "${0}: running migrations."
python manage.py migrate --noinput


echo "${0}: collecting statics."
python manage.py collectstatic --noinput


cp ./static/default.png ./addictions_icons


cp ./static/default.png ./specialists


echo "${0}: creating default data."
python manage.py create_default_data


echo "${0}: starting gunicorn."
echo "from  django.contrib.auth.models import User; print('Admin exists') if User.objects.filter(username='admin').exists() else User.objects.create_superuser('admin', 'sysadmin@some_email.com', '${ADMIN_PASSWORD}')" | python manage.py shell


gunicorn addiction_app.wsgi:application \
    --name addiction_app \
    --bind 0.0.0.0:8000 \
    --timeout 600 \
    --workers 2 \
    --log-level=info \
    --reload
"$@"