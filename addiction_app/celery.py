import os

import celery
from celery import Celery
from celery.schedules import crontab
from celery.task import periodic_task

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'addiction_app.settings')

app = Celery('addiction_app')

CELERY_TIMEZONE = 'UTC'

app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.broker_url = 'redis://redis:6379/0'
app.conf.result_backend = 'redis://redis/1'

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'get_notifications': {
        'task': 'apps.general.tasks.get_notifications',
        'schedule': 15.0
    },
    'reduce_notifications': {
        'task': 'apps.general.tasks.reduce_notifications',
        'schedule': 600.0
    },
}
