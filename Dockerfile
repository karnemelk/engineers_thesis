FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y --no-install-recommends

ADD requirements.txt /code/

WORKDIR /code

RUN pip install -r requirements.txt

ADD . /code/

RUN chmod +x *.sh

EXPOSE 8000