\babel@toc {polish}{}
\babel@toc {polish}{}
\contentsline {chapter}{Streszczenie}{5}{chapter*.2}
\contentsline {chapter}{Wst\k ep}{11}{chapter*.4}
\contentsline {chapter}{\numberline {1}Istniej\k ace rozwi\k azania}{13}{chapter.1}
\contentsline {section}{\numberline {1.1}Alky Recovery}{13}{section.1.1}
\contentsline {section}{\numberline {1.2}Quitzilla}{15}{section.1.2}
\contentsline {section}{\numberline {1.3}QuitNow}{16}{section.1.3}
\contentsline {section}{\numberline {1.4}Podsumowanie}{17}{section.1.4}
\contentsline {chapter}{\numberline {2}Wykorzystane technologie}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}IDE PyCharm}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}Python}{20}{section.2.2}
\contentsline {section}{\numberline {2.3}Django}{20}{section.2.3}
\contentsline {section}{\numberline {2.4}HTML/CSS/JavaScript/JQuery}{21}{section.2.4}
\contentsline {section}{\numberline {2.5}Docker}{22}{section.2.5}
\contentsline {section}{\numberline {2.6}Celery}{22}{section.2.6}
\contentsline {section}{\numberline {2.7}Redis}{23}{section.2.7}
\contentsline {section}{\numberline {2.8}PostgreSQL}{23}{section.2.8}
\contentsline {chapter}{\numberline {3}Projekt systemu}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Opis architektury}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Opis komponent\'ow aplikacji}{27}{section.3.2}
\contentsline {section}{\numberline {3.3}Diagramy i~schematy}{28}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Schemat bazy danych/modeli}{28}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Diagram przypadk\'ow u\.zycia}{32}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Algorytm kalendarza}{33}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Opis og\'olny funkcjonalno\'sci}{33}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Opis algorytmu}{34}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Przygotowanie kalendarza}{34}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Dodanie daty przerwania}{39}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Usuni\k ecie punktu przerwania}{41}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Podsumowanie}{42}{subsection.3.4.6}
\contentsline {chapter}{\numberline {4}Instrukcja u\.zytkownika}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}Pasek nawigacyjny i~strona g\IeC {\l }\'owna}{45}{section.4.1}
\contentsline {section}{\numberline {4.2}Rejestracja i~logowanie}{48}{section.4.2}
\contentsline {section}{\numberline {4.3}Podstawowe funkcjonalno\'sci}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\'Scie\.zki wychodzenia z~na\IeC {\l }og\'ow}{50}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Wiadomo\'sci}{55}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Specjali\'sci}{57}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Forum}{59}{subsection.4.3.4}
\contentsline {section}{\numberline {4.4}Panel administratora}{62}{section.4.4}
\contentsline {chapter}{Podsumowanie}{69}{chapter*.60}
\contentsline {chapter}{Bibliografia}{74}{chapter*.61}
\contentsline {chapter}{Spis rysunk\'ow}{77}{chapter*.62}
\contentsline {chapter}{Spis listing\'ow}{79}{chapter*.63}
